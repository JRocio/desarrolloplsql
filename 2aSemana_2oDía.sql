/****CURSORES***/
/*Se definen en la zona de declare y es una variable m�s, solo que 
apunta a un conjunto de registros, resultados de un select o una vista.
No hace diferencia entre una vista o una tabla. Tambi�n es temporal. */
DECLARE
    CURSOR cur_depts IS-- Aqi solo lo declaramos 
        SELECT department_id, department_name, location_id 
        FROM departments;
        v_department_id departments.department_id%TYPE;
        v_department_name departments.department_name%TYPE;
        v_location_id  departments.location_id%TYPE;
BEGIN
    OPEN cur_depts; --Ejecutamos el cursor, lo abrimos
    LOOP
    --El FETCH es como, la primera vez obten el primer registro del cursor, la segunda el segundo y as� sucesivamente 
        FETCH cur_depts INTO v_department_id, v_department_name,v_location_id;
        EXIT WHEN cur_depts%NOTFOUND;--Cuando ya no haya mas registros en el cursor 
        DBMS_OUTPUT.PUT_LINE(v_department_id||' '||v_department_name||' '||v_location_id);
    END LOOP;
    CLOSE cur_depts;
END;

DECLARE
    CURSOR cur_emps IS
        SELECT employee_id, last_name, salary 
        FROM employees
        WHERE department_id =30;
    v_empno employees.employee_id%TYPE;
    v_lname employees.last_name%TYPE;
    v_sal employees.salary%TYPE;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_empno, v_lname,v_sal;
        EXIT WHEN cur_emps%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE( v_empno ||' '||v_lname);
    END LOOP;
    CLOSE cur_emps;
END;

DECLARE
    CURSOR cur_emps IS
        SELECT employee_id, last_name FROM employees
        WHERE department_id =10;
    v_empno employees.employee_id%TYPE;
    v_lname employees.last_name%TYPE;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_empno, v_lname;
        DBMS_OUTPUT.PUT_LINE(v_empno || ' ' || v_lname);
        EXIT WHEN cur_emps%NOTFOUND;
    END LOOP;
    CLOSE cur_emps;
END;
/*Usando %ROWYPE**/
DECLARE
    CURSOR cur_emps IS
        SELECT * FROM employees
        WHERE department_id = 30;
    v_emp_record cur_emps%ROWTYPE;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_record;
        EXIT WHEN cur_emps%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(v_emp_record.employee_id || ' - '
        || v_emp_record.last_name|| ' - '||v_emp_record.salary);
    END LOOP;
    CLOSE cur_emps;
END;
/* cur_emps%ROWCOUNT  Para contar numero de registros */
DECLARE
    CURSOR cur_emps IS
        SELECT employee_id, last_name FROM employees;
        v_emp_record cur_emps%ROWTYPE;
        v_rowcount NUMBER;
BEGIN
    OPEN cur_emps;
    /*v_rowcount :=cur_emps%ROWCOUNT;
    DBMS_OUTPUT.PUT_LINE(v_rowcount);  ---En este caso muestra Cero.*/
    LOOP
        FETCH cur_emps INTO v_emp_record;
        EXIT WHEN   cur_emps%ROWCOUNT > 10 OR cur_emps%NOTFOUND;
        /*v_rowcount :=cur_emps%ROWCOUNT;
        DBMS_OUTPUT.PUT_LINE(v_rowcount);-- Va contado y mostrando de uno en uno hasta  107*/
        DBMS_OUTPUT.PUT_LINE(v_emp_record.employee_id || ' '
        || v_emp_record.last_name);
    END LOOP;
    v_rowcount :=cur_emps%ROWCOUNT;
    DBMS_OUTPUT.PUT_LINE(v_rowcount);--Muestra 107
    CLOSE cur_emps;
END;


DECLARE
    CURSOR cur_emps IS
    SELECT employee_id, last_name FROM employees
    WHERE department_id = 50;
BEGIN
    FOR v_emp_record IN cur_emps LOOP
    DBMS_OUTPUT.PUT_LINE(v_emp_record.employee_id || ' '
    || v_emp_record.last_name);
END LOOP;
END;


BEGIN
    FOR v_emp_record IN (SELECT employee_id, last_name
    FROM employees WHERE department_id = 50)
    LOOP
        DBMS_OUTPUT.PUT_LINE(v_emp_record.employee_id || ' '
        || v_emp_record.last_name);
    END LOOP;
END;

/* En ejecuci�n del cursor determinas cual ser� la consulta que har�
pasandole un parametro que usar� para el WHERE en el SELECT*/
DECLARE
    CURSOR cur_country (p_region_id NUMBER) IS
    SELECT country_id, country_name
    FROM countries
    WHERE region_id = p_region_id;
    v_country_record cur_country%ROWTYPE;
BEGIN
    OPEN cur_country (1);
        LOOP
            FETCH cur_country INTO v_country_record;
            EXIT WHEN cur_country%NOTFOUND;
            DBMS_OUTPUT.PUT_LINE(v_country_record.country_id || ' '
            || v_country_record.country_name);
        END LOOP;
    CLOSE cur_country;
END;

/*UPDATE WITH CURSORS*/ 
DECLARE
    CURSOR cur_emps IS
    SELECT employee_id, salary FROM copy_employees
    WHERE salary <= 20000 FOR UPDATE NOWAIT;
    v_emp_rec cur_emps%ROWTYPE;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_rec;
        EXIT WHEN cur_emps%NOTFOUND;
        UPDATE copy_employees
        SET salary = v_emp_rec.salary*1.1
        WHERE CURRENT OF cur_emps;
    END LOOP;
    CLOSE cur_emps;
END;

DECLARE
    CURSOR cur_eds IS
    SELECT employee_id, salary, department_name
    FROM copy_employees e, copy_departments d
    WHERE e.department_id = d.department_id
    FOR UPDATE OF salary NOWAIT;
    -- Bloquea la tabla departments, as� que solo se puede modificar copy_employees
BEGIN
    FOR v_eds_rec IN cur_eds LOOP
        UPDATE copy_employees
        SET salary = v_eds_rec.salary * 1.1
        WHERE CURRENT OF cur_eds;
    END LOOP;
END;

/*********EXCEPCIONES*****/
DECLARE
    v_country_name countries.country_name%TYPE := 'Korea, South';
    v_reg_id countries.region_id%TYPE;
BEGIN
    SELECT region_id INTO v_reg_id
        FROM countries WHERE country_name = v_country_name;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN --Si el registro no tiene datos
        DBMS_OUTPUT.PUT_LINE ('Country name, ' || v_country_name || ',
        cannot be found. Re-enter the country name using the correct
        spelling.');
END;

DECLARE
    v_lname employees.last_name%TYPE;
BEGIN
    SELECT last_name INTO v_lname
    FROM employees WHERE job_id = 'ST_CLERK';
    DBMS_OUTPUT.PUT_LINE('The last name of the ST_CLERK is: ' || v_lname);
    EXCEPTION
        WHEN TOO_MANY_ROWS THEN
        DBMS_OUTPUT.PUT_LINE ('Your select statement retrieved multiple rows.
        Consider using a cursor.');
END;

DECLARE
    e_insert_excep EXCEPTION;
    --Son excepciones no definidas y as� las defines tu mismo, para posteriormente manejarlas
    PRAGMA EXCEPTION_INIT(e_insert_excep, -01400);
BEGIN
    INSERT INTO departments(department_id, department_name)
    VALUES (280, NULL);
    EXCEPTION
        WHEN e_insert_excep
        THEN
        DBMS_OUTPUT.PUT_LINE('INSERT FAILED');
END;

/*Definiendo nustras propias excepciones*/
DECLARE
    e_invalid_department EXCEPTION;
    v_name VARCHAR2(20):='Accounting';
    v_deptno NUMBER := 27;
BEGIN
    UPDATE departments
    SET department_name = v_name
    WHERE department_id = v_deptno;
    IF SQL%NOTFOUND THEN RAISE e_invalid_department;
    END IF;
    EXCEPTION
    WHEN e_invalid_department
    THEN DBMS_OUTPUT.PUT_LINE('No such department id.');
END;


DECLARE
    v_mgr PLS_INTEGER := 27;
    v_employee_id employees.employee_id%TYPE;
BEGIN
    SELECT employee_id INTO v_employee_id
    FROM employees
    WHERE manager_id = v_mgr;
    DBMS_OUTPUT.PUT_LINE('Employee #' || v_employee_id ||
    ' works for manager #' || v_mgr || '.');
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20201,'This manager has no employees');
        WHEN TOO_MANY_ROWS THEN
        RAISE_APPLICATION_ERROR(-20202,'Too many employees were found.');
END;
/*Excepciones en bloques anidados*/
DECLARE
    v_last_name employees.last_name%TYPE;
BEGIN
    BEGIN
        SELECT last_name INTO v_last_name
        FROM employees WHERE employee_id = 999;
        DBMS_OUTPUT.PUT_LINE('Message 1');
        EXCEPTION
            WHEN TOO_MANY_ROWS THEN --Si lo cambiamos por NO_DATA_FOUND entonces Mensaje 2 y 3
            DBMS_OUTPUT.PUT_LINE('Message 2');
    END;
    DBMS_OUTPUT.PUT_LINE('Message 3');
    EXCEPTION
    WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Message 4');
END;

/*Como la excepcion esta declarada, el bloque exterior no puede tratarla
debido al nivel de acceso*/
BEGIN
    DECLARE
    e_myexcep EXCEPTION;--Declara dentro del BEGIN
    BEGIN
        RAISE e_myexcep;
        DBMS_OUTPUT.PUT_LINE('Message 1');
        EXCEPTION
        WHEN TOO_MANY_ROWS THEN
        DBMS_OUTPUT.PUT_LINE('Message 2');
    END;
    DBMS_OUTPUT.PUT_LINE('Message 3');
    EXCEPTION
        WHEN e_myexcep THEN
        DBMS_OUTPUT.PUT_LINE('Message 4');
END;
/*Ahora si sale el mensaje 4 porque se declaro la excepci�n de manera 
global y ahora si hay acceso a ella en cualquier bloque*/
DECLARE
    e_myexcep EXCEPTION;
BEGIN
    BEGIN
        RAISE e_myexcep;
        DBMS_OUTPUT.PUT_LINE('Message 1');
        EXCEPTION
        WHEN TOO_MANY_ROWS THEN
        DBMS_OUTPUT.PUT_LINE('Message 2');
    END;
    DBMS_OUTPUT.PUT_LINE('Message 3');
    EXCEPTION
    WHEN e_myexcep THEN
    DBMS_OUTPUT.PUT_LINE('Message 4');
END;