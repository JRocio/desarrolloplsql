/****PROCEDIMIENTOS ********/
--Aqu� solo compilamos el procedimiento
CREATE OR REPLACE PROCEDURE add_dept IS
    v_dept_id departments.department_id%TYPE;
    v_dept_name departments.department_name%TYPE;
BEGIN
    v_dept_id := 280;
    v_dept_name := 'ST-Curriculum';
    INSERT INTO departments(department_id, department_name)
    VALUES(v_dept_id, v_dept_name);
    DBMS_OUTPUT.PUT_LINE('Inserted '|| SQL%ROWCOUNT || ' row.');
END; 
--Ahora si vamos a ejecutar el procedimiento y posteriormente una consulta
BEGIN
add_dept;
END;
SELECT department_id, department_name FROM departments WHERE department_id=280;

/*Creando un procedimiento y mandandolo llamar en el siguiente*/
CREATE OR REPLACE PROCEDURE raise_salary
    (p_id IN copy_employees.employee_id%TYPE,
    p_percent IN NUMBER)
    IS
        BEGIN
        UPDATE copy_employees
        SET salary = salary * (1 + p_percent/100)
        WHERE employee_id = p_id;
END raise_salary;

CREATE OR REPLACE PROCEDURE process_employees
    IS
        CURSOR emp_cursor IS
        SELECT employee_id
        FROM copy_employees;
BEGIN
    FOR v_emp_rec IN emp_cursor
    LOOP raise_salary(v_emp_rec.employee_id, 10);
    END LOOP;
END process_employees;

BEGIN
process_employees;
END;

/*Usando parametros IN/OUT*/

CREATE OR REPLACE PROCEDURE query_emp(p_id IN employees.employee_id%TYPE,
                                      p_name OUT employees.last_name%TYPE,
                                      p_salary OUT employees.salary%TYPE) 
IS
BEGIN
    SELECT last_name, salary INTO p_name, p_salary
    FROM employees
    WHERE employee_id = p_id;
END query_emp;

DECLARE
    a_emp_name employees.last_name%TYPE;
    a_emp_sal employees.salary%TYPE;
BEGIN
    query_emp(178, a_emp_name, a_emp_sal); --Los parametros de salida del procedimiento se gusrdan en estas variables
    DBMS_OUTPUT.PUT_LINE('Name: ' || a_emp_name);
    DBMS_OUTPUT.PUT_LINE('Salary: ' || a_emp_sal);
END;

/***FUNCIONES***/


CREATE OR REPLACE FUNCTION get_sal(p_id IN employees.employee_id%TYPE) 
    RETURN NUMBER IS
    v_sal employees.salary%TYPE := 0;
BEGIN
    SELECT salary INTO v_sal
    FROM employees 
    WHERE employee_id = p_id;
    RETURN v_sal;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN RETURN NULL;
END get_sal;

--DECLARE v_sal employees.salary%type;
BEGIN
    --v_sal := get_sal(999);
    DBMS_OUTPUT.PUT_LINE('Salary: ' || get_sal(100));
END;

SELECT job_id, get_sal(employee_id) FROM employees;

--OTRA FUNCION
CREATE OR REPLACE FUNCTION valid_dept(p_dept_no departments.department_id%TYPE)
    RETURN BOOLEAN IS v_valid VARCHAR2(1);
BEGIN
    SELECT 'x' INTO v_valid /*Regresa X si hay coincidencia, de lo contrario no regresa nada 
    lo cual har� retornar un false*/
    FROM departments
    WHERE department_id = p_dept_no;
    RETURN(true);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN RETURN(false);
        WHEN OTHERS THEN NULL;
END;

/*No podemos mandar llamar esa funci�n en una sentencia normal de PLSQL
ya que retorna un valor boolean*/
BEGIN
    IF valid_dept(100) THEN
        dbms_output.put_line('TRUE');
    ELSE
        dbms_output.put_line('FALSE');
        DBMS_OUTPUT.PUT_LINE(USER);
    END IF;
END;

/*OTRA FUNCION*/
CREATE OR REPLACE FUNCTION tax(p_value IN NUMBER)
RETURN NUMBER IS
BEGIN
RETURN (p_value * 0.08);
END tax;

SELECT employee_id, last_name, salary, tax(salary) Taxes
FROM employees
WHERE department_id = 50;

/*DATA DICTIONARY*/

SELECT table_name, owner FROM ALL_TABLES;

SELECT object_type, COUNT(*) FROM USER_OBJECTS
GROUP BY object_type;

/*EXCEPCIONES DENTRO DE FUNCIONES*/
CREATE OR REPLACE PROCEDURE add_department(p_name VARCHAR2, p_mgr NUMBER, p_loc NUMBER) IS
BEGIN
    INSERT INTO DEPARTMENTS (department_id,department_name, manager_id, location_id)
        VALUES (DEPARTMENTS_SEQ.NEXTVAL, p_name, p_mgr, p_loc);
        DBMS_OUTPUT.PUT_LINE('Added Dept: ' || p_name);
    EXCEPTION
        WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error adding dept: ' || p_name);
END;


/****PAQUETES***/
CREATE OR REPLACE PACKAGE check_emp_pkg
    IS
        g_max_length_of_service CONSTANT NUMBER := 100;
        PROCEDURE chk_hiredate(p_date IN employees.hire_date%TYPE);
        PROCEDURE chk_dept_mgr(p_empid IN employees.employee_id%TYPE, p_mgr IN employees.manager_id%TYPE);
END check_emp_pkg;

CREATE OR REPLACE PACKAGE manage_jobs_pkg
    IS
        g_todays_date DATE := SYSDATE;
        CURSOR jobs_curs IS
            SELECT employee_id, job_id FROM employees
            ORDER BY employee_id;
        PROCEDURE update_job(p_emp_id IN employees.employee_id%TYPE);
        PROCEDURE fetch_emps(p_job_id IN employees.job_id%TYPE,p_emp_id OUT employees.employee_id%TYPE);
END manage_jobs_pkg;

--Definiendo el cuerpo del primer paquete que creamos
CREATE OR REPLACE PACKAGE BODY check_emp_pkg IS
    PROCEDURE chk_hiredate(p_date IN employees.hire_date%TYPE)
    IS BEGIN
        IF MONTHS_BETWEEN(SYSDATE, p_date) >g_max_length_of_service * 12 THEN
        RAISE_APPLICATION_ERROR(-20201, 'Hiredate Too Old');
        END IF;
    END chk_hiredate;
    PROCEDURE chk_dept_mgr(p_empid IN employees.employee_id%TYPE,p_mgr IN employees.manager_id%TYPE)
    IS BEGIN 
        DBMS_OUTPUT.PUT_LINE(' ');
    END chk_dept_mgr;
END check_emp_pkg;

CREATE OR REPLACE PACKAGE salary_pkg
    IS
        g_max_sal_raise CONSTANT NUMBER := 0.20;
        PROCEDURE update_sal(p_employee_id employees.employee_id%TYPE,p_new_salary employees.salary%TYPE);
END salary_pkg;


