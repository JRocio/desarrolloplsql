/********** TRIGGER**********/

CREATE TABLE LOG_TABLE (USER_ID VARCHAR2(100) NOT NULL , COLUMN1 DATE,
    CONSTRAINT LOG_TABLE_PK PRIMARY KEY (USER_ID,COLUMN1)ENABLE);

/*Se ejecuta de manera autom�tica el bloque de
c�digo anonimo AFTER, BEFORE O INSTEAD OF 
que la acci�n que esta enseguida de cualquiera de 
estas palabras. En este ejemplo,se actualiza un dato 
y posteriormente se hace un registro en log_table */

CREATE OR REPLACE TRIGGER log_sal_change_trigg
    AFTER UPDATE OF salary ON employees
BEGIN
    INSERT INTO log_table (user_id, column1)
    VALUES (USER, SYSDATE);
END;


UPDATE employees
SET salary = salary * 1.1
WHERE employee_id = 100;

