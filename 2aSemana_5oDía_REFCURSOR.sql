--*********************************************************************--
--*************PL\SQL CURSOR VARIABLE WITH REF CURSOR******************--
--*********************************************************************--

/*
Una variable de cursor es una variable que referenc�a a un cursor. Es diferente a 
los cursores implicitos y explicitos porque no est� vinculada a ninguna consulta espec�fica.
Es decir, se puede abrir para cualquier consulta.

Permiten pasar el resultado de una consulta entre bloques PL-SQL porque solo se pasa 
la referencia a ese cursor.

El tipo de datos es REF CURSOR.

El valor de un REF CURSOR es la direcci�n de memoria de un �rea de trabajo de consulta en la BD.

*/

/*Tenemos dos tipos, el primero es strong typed
porque la variable de cursor siempre est� asociada con un tipo 
de registro especifico*/
DECLARE
    TYPE customer_t IS REF CURSOR RETURN customers%ROWTYPE;
    c_customer customer_t;
BEGIN
    --.....
END;

/*El segundo es weak typed, porque no est� asociada con ninguna 
estructura especifica*/
DECLARE 
    TYPE customer_t IS REF CURSOR;
    c_customer customer_t;
BEGIN
    DBMS_OUTPUT.PUT_LINE("asdadasdas");
END;

/*
A partir de Oracle 9i se puede usar SYS_REFCURSOR que es un tipo debil 
*/
DECLARE
    c_customer SYS_REFCURSOR;
BEGIN
    --...
END;


/*EJEMPLOS*/
--Obtiene los reportes de un administrador en funci�n del id de un administador
--Retorna un REF CURSOR weak
CREATE OR REPLACE FUNCTION get_direct_reports(in_manager_id IN employees.manager_id%TYPE)
RETURN SYS_REFCURSOR
AS
   c_direct_reports SYS_REFCURSOR;
BEGIN
   OPEN c_direct_reports FOR 
   SELECT 
      employee_id, 
      first_name, 
      last_name, 
      email
   FROM 
      employees 
   WHERE 
      manager_id = in_manager_id 
   ORDER BY 
         first_name,   
         last_name;
   RETURN c_direct_reports;
END;

/*Ahora lo mandamos a llamar desde un bloque anonimo para mostrar los
informes del administrador 101*/
DECLARE
   c_direct_reports SYS_REFCURSOR;
   l_employee_id employees.employee_id%TYPE;
   l_first_name  employees.first_name%TYPE;
   l_last_name   employees.last_name%TYPE;
   l_email       employees.email%TYPE;
BEGIN
   c_direct_reports := get_direct_reports(101); 
   
   LOOP
      FETCH
         c_direct_reports INTO l_employee_id,l_first_name,l_last_name,l_email;
      EXIT WHEN c_direct_reports%notfound;
      dbms_output.put_line(l_first_name || ' ' || l_last_name || ' - ' ||    l_email||'@oracle.com' );
   END LOOP;
   CLOSE c_direct_reports;
END;