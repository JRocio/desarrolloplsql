
/******CREACI�N DE PAQUETES SIN BODY****/
CREATE OR REPLACE PACKAGE global_consts IS
mile_to_kilo CONSTANT NUMBER := 1.6093;
kilo_to_mile CONSTANT NUMBER := 0.6214;
yard_to_meter CONSTANT NUMBER := 0.9144;
meter_to_yard CONSTANT NUMBER := 1.0936;
END global_consts;

GRANT EXECUTE ON global_consts TO PUBLIC;

DECLARE
distance_in_miles NUMBER(5) := 5000;
distance_in_kilo NUMBER(6,2);
BEGIN
distance_in_kilo :=
distance_in_miles * global_consts.mile_to_kilo;
DBMS_OUTPUT.PUT_LINE(distance_in_kilo);
END;

/***OTRO PAQUETE*/
CREATE OR REPLACE PACKAGE our_exceptions IS
    e_cons_violation EXCEPTION;
    PRAGMA EXCEPTION_INIT (e_cons_violation, -2292);
    e_value_too_large EXCEPTION;
    PRAGMA EXCEPTION_INIT (e_value_too_large, -1438);
END our_exceptions;

GRANT EXECUTE ON our_exceptions TO PUBLIC; 

CREATE TABLE excep_test (number_col NUMBER(3));

BEGIN
    INSERT INTO excep_test (number_col) VALUES (100);
    EXCEPTION
    WHEN our_exceptions.e_value_too_large THEN
    DBMS_OUTPUT.PUT_LINE('Value too big for column data type');
END;
SELECT number_col FROM excep_test; --Podemos ver lo insetaado en la tabla 

/*NUEVO PAQUETE*/
CREATE OR REPLACE PACKAGE taxes_pkg IS
FUNCTION tax (p_value IN NUMBER) RETURN NUMBER;
END taxes_pkg;

CREATE OR REPLACE PACKAGE BODY taxes_pkg IS
FUNCTION tax (p_value IN NUMBER) RETURN NUMBER IS
v_rate NUMBER := 0.08;
BEGIN
RETURN (p_value * v_rate);
END tax;
END taxes_pkg;

SELECT taxes_pkg.tax(salary), salary, last_name
FROM employees; --Fue necesario indicar la ruta de la funci�n

--------------------------------------------------
CREATE OR REPLACE PROCEDURE sel_one_emp(p_emp_id IN employees.employee_id%TYPE,
              /*Cursor*/       p_emprec OUT employees%ROWTYPE)
IS BEGIN
    SELECT * INTO p_emprec FROM employees
    WHERE employee_id = p_emp_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN DBMS_OUTPUT.PUT('No encontrado ');
END sel_one_emp;

DECLARE
    v_emprec employees%ROWTYPE;
    --Esta variable recibira todo un registro
BEGIN
    sel_one_emp(100, v_emprec);  
    dbms_output.put_line(v_emprec.last_name);
END;
--------------------------------------------------
/*****CURSOR EN PAQUETES****/
CREATE OR REPLACE PACKAGE curs_pkg IS
    CURSOR emp_curs IS SELECT employee_id 
    FROM employees
    ORDER BY employee_id;
    PROCEDURE open_curs;
    FUNCTION fetch_n_rows(n NUMBER := 1) RETURN BOOLEAN;
    PROCEDURE close_curs;
END curs_pkg;

CREATE OR REPLACE PACKAGE BODY curs_pkg IS 
    PROCEDURE open_curs IS
        BEGIN
            IF NOT emp_curs%ISOPEN THEN OPEN emp_curs; END IF;
    END open_curs;
    FUNCTION fetch_n_rows(n NUMBER := 1) RETURN BOOLEAN IS
        emp_id employees.employee_id%TYPE;
        BEGIN
            FOR count IN 1 .. n LOOP
                FETCH emp_curs INTO emp_id;
                EXIT WHEN emp_curs%NOTFOUND;
                DBMS_OUTPUT.PUT_LINE('Id: ' ||(emp_id));
            END LOOP;
            RETURN emp_curs%FOUND;
    END fetch_n_rows;
    PROCEDURE close_curs IS BEGIN
        IF emp_curs%ISOPEN THEN CLOSE emp_curs; END IF;
    END close_curs;
END curs_pkg;

DECLARE
    v_more_rows_exist BOOLEAN := TRUE;
    contador NUMBER :=1;
BEGIN
    curs_pkg.open_curs; --1
    LOOP
        v_more_rows_exist := curs_pkg.fetch_n_rows(contador); --2
        contador:=contador+1;
        DBMS_OUTPUT.PUT_LINE('-------');
    EXIT WHEN NOT v_more_rows_exist;
    END LOOP;
curs_pkg.close_curs; --3
END;

/**********RENDIMIENTO*********/

/***BULK AND FORALL*/

delete from copy_employees;

CREATE OR REPLACE PROCEDURE insert_emps IS
TYPE t_emps IS TABLE OF employees%ROWTYPE
INDEX BY BINARY_INTEGER;
v_emptab t_emps;
BEGIN
    SELECT * BULK COLLECT INTO v_emptab FROM employees;
    FORALL i IN v_emptab.FIRST..v_emptab.LAST
        INSERT INTO copy_employees VALUES v_emptab(i);
        FOR j IN v_emptab.FIRST..v_emptab.LAST LOOP
        DBMS_OUTPUT.PUT_LINE('Inserted: '
        || j || ' '||SQL%BULK_ROWCOUNT(j)|| 'rows');
    END LOOP;
END insert_emps;

BEGIN 
insert_emps;
END;


