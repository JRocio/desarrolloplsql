/*BLOQUES ANONIMOS*/
/*Primer programa en PLSQL, como un sysout*/
BEGIN
DBMS_OUTPUT.PUT_LINE('PL/SQL es facil!');
END;

/*Guardamos la fecha en una variabe y en PLSQL es necesario poner 
:= en vez de igual, ya que representa ASIGNACI�N */
DECLARE
    v_date DATE := SYSDATE+2; --Guardamos en una variable
BEGIN
    DBMS_OUTPUT.PUT_LINE(v_date);--Imprimimos esa variable
END;


/*BLOQUES CON NOMBRE,FUNCIONES Y PROCEDIMIENTOS se guardan*/

/*No ejecutamos este bloque de c�digo, solo lo definimos y guardamos
el PROCEDIMIENTO*/
CREATE OR REPLACE PROCEDURE print_date IS v_date VARCHAR2(30);
BEGIN
    SELECT TO_CHAR(SYSDATE+1,'Mon DD, YYYY')
        INTO v_date
        FROM DUAL;
    DBMS_OUTPUT.PUT_LINE(v_date);
END;
 --- Ahora si estamos EJECUTANDO EL PROCEDIMIENTO. 
BEGIN
PRINT_DATE;
END;

/*FUNCION, llamada tomorrow, IN porque indicas que vas a recibir 
ese tipo de dato. Solo la compilamos y guardamos*/
CREATE OR REPLACE FUNCTION tomorrow (p_today IN DATE)--Parametro de entrada tipo fecha
    RETURN DATE IS v_tomorrow DATE; --Variable que vamos a retornar
    BEGIN
        SELECT p_today + 1 --Hace la consulta
        INTO v_tomorrow --Asigna la consulta anterior a esta variable
        FROM DUAL;--No es necesaria una tabla
    RETURN v_tomorrow; --Entregamos la variaable
END;

--Recibe la fecha del systema la funci�n
SELECT TOMORROW(SYSDATE) AS "Tomorrow's Date"
FROM DUAL;
--Ahora la probamos a travez de un bloque de c�digo
BEGIN
    DBMS_OUTPUT.PUT_LINE(TOMORROW(SYSDATE+1));
END;

/*******VARIABLES***************/

DECLARE
    v_myname VARCHAR2(20);
BEGIN
    DBMS_OUTPUT.PUT_LINE('My name is: '||v_myname);
        v_myname := 'John';
    DBMS_OUTPUT.PUT_LINE('My name is: '||v_myname);
END;

DECLARE
    v_date VARCHAR2(30);
BEGIN
    SELECT TO_CHAR(SYSDATE) INTO v_date FROM DUAL;
    DBMS_OUTPUT.PUT_LINE(v_date);
END;

/*Creando la funcion,revision de creaci�n de funciones y uso de variables */
CREATE OR REPLACE FUNCTION num_characters (p_string IN VARCHAR2)
    RETURN INTEGER IS v_num_characters INTEGER;
BEGIN
    SELECT LENGTH(p_string) --Saca la longitud del string que recibe
        INTO v_num_characters --Guarda la consulta anterior
        FROM DUAL;
    RETURN v_num_characters;
END;
--Ejecuntando la funcion anterior
DECLARE
    v_length_of_string INTEGER;
BEGIN
    --Mandamos llamar la funci�n y enviamos el parametro VARCHAR2
    v_length_of_string := num_characters('Oracle Corporation');
    DBMS_OUTPUT.PUT_LINE(v_length_of_string);
END;

/***CONVERSI�N IMPLICITA****/
--Sumo un Numero y un VARCHAR2
DECLARE
    v_salary NUMBER(6) := 6000;
    v_sal_increase VARCHAR2(5) := '1000';
    v_total_salary v_salary%TYPE;
BEGIN
    v_total_salary := v_salary + v_sal_increase;
    DBMS_OUTPUT.PUT_LINE(v_total_salary);
END;


/****BLOQUES ANIDADOS**********INNER Y OUTER VARIABLES */

DECLARE
    v_father_name VARCHAR2(20):='Patrick';
    v_date_of_birth DATE:='20-Abr-1972';
BEGIN
    DECLARE
        v_child_name VARCHAR2(20):='Mike';
    BEGIN
        DBMS_OUTPUT.PUT_LINE('Father''s Name: '||v_father_name);
        DBMS_OUTPUT.PUT_LINE('Date of Birth: '||v_date_of_birth);
        DBMS_OUTPUT.PUT_LINE('Child''s Name: '||v_child_name);
    END;
DBMS_OUTPUT.PUT_LINE('Date of Birth: '||v_date_of_birth);
END;

DECLARE
    v_first_name VARCHAR2(20);
    v_last_name VARCHAR2(20);
BEGIN
    BEGIN
        v_first_name := 'Carmen';
        v_last_name := 'Miranda';
    DBMS_OUTPUT.PUT_LINE(v_first_name || ' ' || v_last_name);
    END;
    DBMS_OUTPUT.PUT_LINE(v_first_name || ' ' || v_last_name);
END;

<<externo>> --Nos ayuda, para poder imprimir variables del bloque donde la declaramos
DECLARE
    v_father_name VARCHAR2(20):='Patrick';
    v_date_of_birth DATE:='20-Abr-1972';
BEGIN
   -- <<externo>> Podemos usar la etiqueta en el nivel que se desee, solo para ver el nivel de aacceso
    DECLARE
        v_child_name VARCHAR2(20):='Mike';
        v_date_of_birth DATE:='12-Dec-2002';
    BEGIN
        DBMS_OUTPUT.PUT_LINE('Father''s Name: ' || v_father_name);
        DBMS_OUTPUT.PUT_LINE('Date of Birth: ' || externo.v_date_of_birth);
        DBMS_OUTPUT.PUT_LINE('Child''s Name: ' || v_child_name);
        DBMS_OUTPUT.PUT_LINE('Date of Birth: ' || v_date_of_birth);
    END;
END;

/*****CURSORES****/
--**CURSORES IMPLICITOS
-- La penultima linea nos ayud� a saber cuantas lineas fueron afectadas
DECLARE
    v_deptno copy_employees.department_id%TYPE := 50;
BEGIN
    DELETE FROM copy_employees
    WHERE department_id = v_deptno;
    DBMS_OUTPUT.PUT_LINE(SQL%ROWCOUNT || ' rows deleted.');
END;


/*****IF,ELSE,... CLAUSES***/


DECLARE
    v_out_var VARCHAR2(15);
    v_in_var NUMBER := 20;
BEGIN
    v_out_var :=CASE v_in_var
        WHEN 1 THEN 'Low value'
        WHEN v_in_var THEN 'Same value'
        WHEN 20 THEN 'Middle value'
        ELSE 'Other value'
END;
DBMS_OUTPUT.PUT_LINE(v_out_var);
END;

/*****LOOP, WHILE, FOR***/
BEGIN
FOR v_outerloop IN 1..3 LOOP
    FOR v_innerloop IN REVERSE 1..5 LOOP
        DBMS_OUTPUT.PUT_LINE('Outer loop is: ' ||v_outerloop ||' and inner loop is: ' ||v_innerloop);
    END LOOP;
END LOOP;
END; 

